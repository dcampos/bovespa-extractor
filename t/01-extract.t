#!perl -T
use 5.006;
use strict;
use warnings FATAL => 'all';
use Test::More;

use Bovespa::Extractor;

plan tests => 2;

my $extractor = new Bovespa::Extractor(
    files => [ 't/COTAHIST_D05012010.ZIP' ],
    stocks => { 'PETR4' => 1 }
);

my $quotes = $extractor->extract;

ok(scalar @$quotes > 0, 'length is greater than 0');
ok($quotes->[0]->[0] eq 'PETR4', 'code is valid');

# diag( "Testing Bovespa::Extractor $Bovespa::Extractor::VERSION, Perl $], $^X" );


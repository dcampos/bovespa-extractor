package Bovespa::Extractor;

# Autor: Darlan P. de Campos
# Versão: 0.1
# Licença: BSD

use Moose;
use Carp;
use IO::File;
use IO::Uncompress::Unzip;

our $VERSION = '0.1';

has files => (
    isa      => 'ArrayRef[Str]',
    is       => 'ro',
    required => 1,
);

has stocks => (
    isa     => 'HashRef[Int]',
    is      => 'ro',
    default => sub { {} },
);

has templ => (
    isa     => 'Str',
    is      => 'ro',
    default => "x2A8x2A12x32A13A13A13x13A13x31A18",
);

sub extract {
    my $self = shift;

    return unless scalar %{$self->stocks};

    my $fh;
    my $quotes = [];

    foreach my $f (@{$self->files}) {
        $fh = $f =~ /\.zip$/i
            ? IO::Uncompress::Unzip->new( $f )
            : IO::File->new( $f ) or croak $!;

        push @{$quotes}, $self->_parse_file( $fh );
    }

    undef $fh;

    return $quotes;
}

sub _parse_file {
    my ($self, $file) = @_;

    my @data;

    while (my $line = <$file>) {
        my ($date, $code, $open, $high, $low, $close, $volume) =
            (unpack($self->templ, $line));

        next unless $self->stocks->{$code};

        $date = sprintf("%.4d-%.2d-%.2d", unpack('A4A2A2', $date));

        push @data, [$code, $date, $open/100, $high/100, $low/100,
            $close/100, int $volume];
    }

    close $file;

    return @data;
}

__PACKAGE__->meta->make_immutable;
no Any::Moose;

__END__

=head1 NAME

Bovespa::Extractor - extract quotes from Bovespa's historical data files

=head1 VERSION

Version 0.1

=head1 SYNOPSIS

    use Bovespa::Extractor;
    use Data::Dumper;

    my $extr = new Bovespa::Extractor(
        files  => [ map { $path . $_ } @files ],
        stocks => \%stocks,
    );

    print Dumper $extr->extract();

=head1 DESCRIPTION

Bovespa provides freely downloadable historical data files (although it requires signing on in order for you to get access to them). This module helps you in the proccess of extracting quote information from those files.

=head1 METHODS

=head2 new( files => \@files, stocks => \%stocks )

The constructor method receives two parameters: C<files>, a array ref to a list of files, including path; and C<stocks>, which are the symbols you want to extract. If a empty hash is passed as stocks, no quote is retrieved at all.

=head2 extract()

This method that actually retrieves the quotes and returns them as a array ref.

=head1 LICENCE


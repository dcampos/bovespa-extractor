package Bovespa::Util;
# 
# Autor: Darlan P. de Campos
# Vers�o: 0.1
# Licen�a: BSD

use warnings;
use strict;

use Carp;
use DateTime;

our (@ISA, @EXPORT_OK);
@ISA = qw( Exporter );
@EXPORT_OK = qw( file_range );

sub file_range {
    my ($start, $end) = @_;
    my @files;

    while ($start <= $end) {

        if ($start->day == 1) {
            if ($start->month == 1 && $end >= _get_last_day( 12, $start->year ) ) {
                push @files, $start->strftime("COTAHIST_A%Y.ZIP");
                $start->add( years => 1 );
            } elsif ($end >= _get_last_day( $start->month, $start->year)) {
                push @files, $start->strftime("COTAHIST_M%m%Y.ZIP");
                $start->add( months => 1 );
            } else {
                push @files, $start->strftime("COTAHIST_D%d%m%Y.ZIP")
                    unless $start->dow > 5;
                $start->add( days => 1 );
            }
        } else {
            push @files, $start->strftime("COTAHIST_D%d%m%Y.ZIP")
                unless $start->dow > 5;
            $start->add( days => 1 );
        }
    }

    return @files;
}


sub _get_last_day {
    return DateTime->last_day_of_month( month => $_[0], year => $_[1] );
}

1;

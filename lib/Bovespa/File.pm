package Bovespa::File;

use Moose;
use IO::File;
use File::Basename;
use IO::Uncompress::Unzip;

has filename => (
    isa => 'Str',
    is  => 'ro',
);

has fh => (
    isa        => 'IO::File',
    is         => 'ro',
    lazy_build => 1,
    init_arg   => undef,
);

sub _build_fh {
    my $self = shift;

    return $self->filename =~ /zip$/i
        ? IO::Uncompress::Unzip->new( $self->filename )
        : IO::File->new( $self->filename ) or croak $!;
}

__PACKAGE__->meta->make_immutable();
no Moose;
